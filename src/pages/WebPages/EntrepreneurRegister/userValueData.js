import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Link } from "react-router-dom";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/stepThree.css";

export default function EntrepreneurRegisterThree() {
  const now = 60;
  return (
    <div id="stepThree">
      <Header />
      <div className="mainThree">
        <h3>Cadastro de empreendedor - 3/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentThree">
          <form action="">
            <label htmlFor="">Valor desejado:</label>
            <input type="text" />

            <label htmlFor="">Como soube da Firgun?:</label>
            <select name="" id="">
              <option value="selected">Selecione</option>
              <option value="">Amigos</option>
              <option value="">A banca</option>
              <option value="">AfroBusiness Brasil</option>
              <option value="">Afropolitan</option>
              <option value="">Agência Solano Trindade</option>
              <option value="">Aliança Empreendedora</option>
              <option value="">Anpecom</option>
              <option value="">ArqCoop+</option>
              <option value="">ASPLANDE</option>
              <option value="">Associação MIGRAFLIX</option>
              <option value="">
                AMIP - Associação Moda Íntima e Praia de Taiobeiras
              </option>
              <option value="">Aventura de Construir</option>
              <option value="">Brado Edu</option>
              <option value="">CIEDS</option>
              <option value="">COLETANDO SOLUÇÕES TECNOLOGIA LTDA</option>
              <option value="">Consulado da Mulher</option>
              <option value="">Empreende Aí</option>
              <option value="">Feira Preta</option>
              <option value="">GAMBIARRA Espaço Criativo e Coletivo</option>
              <option value="">Google</option>
              <option value="">INSTITUTO ALINHA </option>
              <option value="">Makeda Cosméticos</option>
              <option value="">Mensageiros da Esperança</option>
              <option value="">Outros</option>
              <option value="">Repagina.me</option>
              <option value="">SINCOTEC-MT</option>
              <option value="">TEAR - estratégia em inovação e expansão</option>
            </select>

            <label htmlFor="">Qual o motivo do crédito?:</label>
            <select name="" id="">
              <option value="selected">Selecione</option>
              <option value="">Capital de giro</option>
              <option value="">Infraestrutura</option>
              <option value="">Marketing</option>
              <option value="">Pagar divida</option>
              <option value="">Contratação</option>
              <option value="">Estoque</option>
              <option value="">Outros</option>
            </select>

            <label htmlFor="">Qual seu segmento de mercado?:</label>
            <select name="" id="">
              <option value="selected">Selecione</option>
              <option value="">Salão de beleza</option>
              <option value="">Alimentação</option>
              <option value="">Artesanato</option>
              <option value="">Educação</option>
              <option value="">Moda</option>
              <option value="">Loja (varejo)</option>
              <option value="">Prestação de serviço</option>
              <option value="">Turismo</option>
              <option value="">Agricultura</option>
              <option value="">Outro</option>
            </select>
          </form>

          <div className="btnWay">
            <button>
              <Link to="/entrepreneurRegister/stepTwo" className="btn">
                Voltar
              </Link>
            </button>

            <button>
              <Link to="/entrepreneurRegister/stepFour" className="btn">
                Continuar
              </Link>
            </button>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
