import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/stepFour.css";

export default function EntrepreneurRegisterFour() {
  const now = 100;
  return (
    <div id="stepFour">
      <Header />
      <div className="mainFour">
        <h3>Cadastro de empreendedor - 5/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentFour">
          <h2>Cadastro realizado com sucesso!</h2>
        </div>
      </div>
      <Footer />
    </div>
  );
}
