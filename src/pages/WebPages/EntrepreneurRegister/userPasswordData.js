import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Link } from "react-router-dom";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/stepFour.css";

export default function EntrepreneurRegisterFour() {
  const now = 80;
  return (
    <div id="stepFour">
      <Header />
      <div className="mainFour">
        <h3>Cadastro de empreendedor - 4/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentFour">
          <form action="">
            <label htmlFor="">Senha:</label>
            <input type="password" />

            <label htmlFor="">Confirmar senha:</label>
            <input type="password" />
          </form>

          <div className="btnWay">
            <button>
              <Link to="/entrepreneurRegister/stepThree" className="btn">
                Voltar
              </Link>
            </button>

            <button>
              <Link to="/entrepreneurRegister/success" className="btn">
                Continuar
              </Link>
            </button>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
