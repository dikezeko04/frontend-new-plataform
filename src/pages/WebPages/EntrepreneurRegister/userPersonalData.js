import React from "react";
import InputMask from "react-input-mask";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Link } from "react-router-dom";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/stepTwo.css";

export default function EntrepreneurRegisterTwo() {
  const now = 40;
  return (
    <div id="stepTwo">
      <Header />
      <div className="mainTwo">
        <h3>Cadastro de empreendedor - 2/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentTwo">
          <form action="">
            <label htmlFor="">Nº de telefone:</label>
            <InputMask mask="+55 (99) 99999-9999" type="text" required />

            <label htmlFor="">CEP:</label>
            <InputMask mask="99999-999" type="text" required />

            <label htmlFor="">Endereço:</label>
            <input type="text" />

            <label htmlFor="">Número:</label>
            <input type="text" />

            <label htmlFor="">Complemento:</label>
            <input type="text" />

            <label htmlFor="">Cidade:</label>
            <input type="text" />

            <label htmlFor="">Estado:</label>
            <input type="text" />

            <label htmlFor="">Bairro:</label>
            <input type="text" />
          </form>

          <div className="btnWay">
            <button>
              <Link to="/entrepreneurRegister/stepOne" className="btn">
                Voltar
              </Link>
            </button>

            <button>
              <Link to="/entrepreneurRegister/stepThree" className="btn">
                Continuar
              </Link>
            </button>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
