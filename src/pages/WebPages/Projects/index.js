import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { FaAngleDoubleLeft, FaAngleDoubleRight } from "react-icons/fa";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";
// import Carousel from "../../../components/carousel/Carousel";
import CardCarousel from "../../../components/carousel/cardCarousel/CardCarousel";

import api from "../../../services/api";

import "./styles/projects.css";
import "./styles/projectsResponsive.css";

export default function Projects() {
  const [projectsInProgress, setProjectsInProgress] = useState([]);
  const [projectsCompleted, setProjectsCompleted] = useState([]);

  useEffect(() => {
    api.get("projects/in-progress").then((response) => {
      setProjectsInProgress(response.data);
    });
    api.get("projects/completed").then((response) => {
      setProjectsCompleted(response.data);
    });
  }, []);

  return (
    <div>
      <Header />
      <div className="projectBanner"></div>

      <div className="projectsContentContainer">
        <div className="projectMain">
          <div className="text">
            <h3>Projetos em andamento</h3>
          </div>

          <div className="loadingProjects">
            <div className="projectContainer">
              {projectsInProgress.map((project) => (
                <Link
                  key={project.id}
                  to={`projectDetails?project=${project.id}`}
                >
                  <CardCarousel
                    name={project.name}
                    raised={project.raised}
                    goal={project.goal}
                    description={project.description}
                    date={project.dateLimit}
                    photo={project.__entrepreneur__.avatarUrl}
                    totalInvestments={project.totalSupporters}
                    entrepreneurName={
                      project.__entrepreneur__.firstName +
                      " " +
                      project.__entrepreneur__.lastName
                    }
                  />
                </Link>
              ))}
            </div>
            <div className="optionsButttons">
              <button>
                <FaAngleDoubleLeft size={20} />
                Anterior
              </button>
              <strong>4 de 10</strong>
              <button>
                Próximo
                <FaAngleDoubleRight size={20} />
              </button>
            </div>
          </div>

          <div className="text">
            <h3>Projetos concluídos</h3>
          </div>

          <div className="finishedProjects">
            <div className="projectContainer">
              {projectsCompleted.map((project) => (
                <Link key={project.id} to={`projectDetails?=${project.id}`}>
                  <CardCarousel
                    name={project.name}
                    raised={project.raised}
                    goal={project.goal}
                    description={project.description}
                    date={project.dateLimit}
                    photo={project.__entrepreneur__.avatarUrl}
                    totalInvestments={project.totalSupporters}
                    entrepreneurName={
                      project.__entrepreneur__.firstName +
                      " " +
                      project.__entrepreneur__.lastName
                    }
                  />
                </Link>
              ))}
            </div>
            <div className="optionsButttons">
              <button>
                <FaAngleDoubleLeft size={20} />
                Anterior
              </button>
              <strong>4 de 10</strong>
              <button>
                Próximo
                <FaAngleDoubleRight size={20} />
              </button>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
