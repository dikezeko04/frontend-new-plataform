import React, { useState, useEffect } from "react";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import PartnerCard from "../../../components/partnerCard/PartnerCard";

import api from "../../../services/api";

import "./styles/partners.css";
import "./styles/partnersResponsive.css";

export default function Partners() {
  const [partners, setPartners] = useState([]);

  useEffect(() => {
    api.get("users/all-partners").then((response) => {
      setPartners(response.data);
    });
  }, []);

  return (
    <div>
      <Header />

      <div className="partnersBanner">
        <div className="space">
          <h1>Nos ajude a encontrar empreendedores incríveis.</h1>
          <button>seja um parceiro</button>
        </div>
      </div>

      <div id="partnersMain">
        <div className="cardsTop">
          {partners.map((partner) => (
            <PartnerCard
              key={partner.id}
              name={partner.companyName}
              city={partner.companyAddressCity}
              description={partner.partnerDescription}
              avatar={partner.avatarUrl}
            />
          ))}
        </div>
      </div>

      <Footer />
    </div>
  );
}
