import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import ProgressBar from "react-bootstrap/ProgressBar";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import { differenceInCalendarDays, parseISO } from "date-fns";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import api from "../../../services/api";

import Teste from "../../../assets/images/teste.jpg";

import "./styles/projectDetails.css";

export default function ProjectDetails() {
  const location = useLocation();

  const [project, setProject] = useState({});
  const [user, setUser] = useState({});

  useEffect(() => {
    async function fetchProject() {
      const projectId = location.search.replace("?project=", "");

      try {
        if (!projectId) {
          throw new Error();
        }

        const response = await api.get(
          `projects/get-project?project_id=${projectId}`
        );

        setProject(response.data);
        setUser(response.data.__entrepreneur__);
      } catch (error) {
        console.log(error);
      }
    }

    fetchProject();
  }, [location.search]);

  const percentage = (Number(project.raised) / Number(project.goal)) * 100;

  return (
    <div id="projectDetails">
      <Header />

      <div id="mainContent">
        <div className="topContent">
          <div className="leftContent">
            <h4>{project.name}</h4>

            <iframe
              title="Entrepreneur video"
              src={project.videoUrl}
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            ></iframe>
          </div>

          <div className="rightContent">
            <div className="entrepreneur">
              <img
                src={!user.avatarUrl ? Teste : user.avatarUrl}
                alt={user.firstName}
              />
              <span>{user.firstName + " " + user.lastName}</span>
            </div>

            <div className="projectData">
              <div className="projectDataItem dataBorder">
                <span>
                  <b>6%</b>
                </span>
                <p>ao ano</p>
              </div>
              <div className="projectDataItem dataBorder">
                <span>
                  <b>{project.totalSupporters}</b>
                </span>
                <p>heróis</p>
              </div>
              <div className="projectDataItem dataBorder">
                <span>
                  <b>
                    {`${differenceInCalendarDays(
                      parseISO(String(project.dateLimit)),
                      Date.now()
                    )}`}
                  </b>
                </span>
                <p>dias</p>
              </div>
              <div className="projectDataItem">
                <span>
                  <b>{project.installmentsPrediction}</b>
                </span>
                <p>parcelas</p>
              </div>
            </div>

            <div className="progressBar">
              <div className="projectGoal">
                <p>R$ {project.raised}</p>
                <p>R$ {project.goal},00</p>
              </div>
              <ProgressBar
                id="bar"
                animated
                now={percentage}
                label={`${percentage}%`}
              />
            </div>

            <Link to={`/projectInvestment?project=${project.id}`}>
              APOIAR ESTE PROJETO
            </Link>
          </div>
        </div>

        <div className="bottomContent">
          <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
            <Tab eventKey="home" title="O projeto">
              <p>{project.pageContent}</p>
            </Tab>
            <Tab eventKey="investment" title="O investimento">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet
                est placerat in egestas erat imperdiet sed euismod. Imperdiet
                nulla malesuada pellentesque elit eget gravida cum sociis
                natoque. Placerat duis ultricies lacus sed. Enim ut tellus
                elementum sagittis vitae. Mattis nunc sed blandit libero
                volutpat sed cras. Amet risus nullam eget felis eget nunc
                lobortis. Gravida in fermentum et sollicitudin ac orci
                phasellus. Commodo viverra maecenas accumsan lacus. Aliquam
                malesuada bibendum arcu vitae elementum curabitur. Porttitor
                massa id neque aliquam. Pellentesque elit ullamcorper dignissim
                cras. Consequat nisl vel pretium lectus. Mattis vulputate enim
                nulla aliquet porttitor lacus luctus.
              </p>
            </Tab>
            <Tab eventKey="impact" title="O impacto social">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet
                est placerat in egestas erat imperdiet sed euismod. Imperdiet
                nulla malesuada pellentesque elit eget gravida cum sociis
                natoque. Placerat duis ultricies lacus sed. Enim ut tellus
                elementum sagittis vitae. Mattis nunc sed blandit libero
                volutpat sed cras. Amet risus nullam eget felis eget nunc
                lobortis. Gravida in fermentum et sollicitudin ac orci
                phasellus. Commodo viverra maecenas accumsan lacus. Aliquam
                malesuada bibendum arcu vitae elementum curabitur. Porttitor
                massa id neque aliquam. Pellentesque elit ullamcorper dignissim
                cras. Consequat nisl vel pretium lectus. Mattis vulputate enim
                nulla aliquet porttitor lacus luctus.
              </p>
            </Tab>
            <Tab eventKey="entrepreneur" title="O empreendedor">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet
                est placerat in egestas erat imperdiet sed euismod. Imperdiet
                nulla malesuada pellentesque elit eget gravida cum sociis
                natoque. Placerat duis ultricies lacus sed. Enim ut tellus
                elementum sagittis vitae. Mattis nunc sed blandit libero
                volutpat sed cras. Amet risus nullam eget felis eget nunc
                lobortis. Gravida in fermentum et sollicitudin ac orci
                phasellus. Commodo viverra maecenas accumsan lacus. Aliquam
                malesuada bibendum arcu vitae elementum curabitur. Porttitor
                massa id neque aliquam. Pellentesque elit ullamcorper dignissim
                cras. Consequat nisl vel pretium lectus. Mattis vulputate enim
                nulla aliquet porttitor lacus luctus.
              </p>
            </Tab>
            <Tab eventKey="partner" title="O(s) parceiro(s)">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet
                est placerat in egestas erat imperdiet sed euismod. Imperdiet
                nulla malesuada pellentesque elit eget gravida cum sociis
                natoque. Placerat duis ultricies lacus sed. Enim ut tellus
                elementum sagittis vitae. Mattis nunc sed blandit libero
                volutpat sed cras. Amet risus nullam eget felis eget nunc
                lobortis. Gravida in fermentum et sollicitudin ac orci
                phasellus. Commodo viverra maecenas accumsan lacus. Aliquam
                malesuada bibendum arcu vitae elementum curabitur. Porttitor
                massa id neque aliquam. Pellentesque elit ullamcorper dignissim
                cras. Consequat nisl vel pretium lectus. Mattis vulputate enim
                nulla aliquet porttitor lacus luctus.
              </p>
            </Tab>
            <Tab eventKey="contact" title="Contato">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet
                est placerat in egestas erat imperdiet sed euismod. Imperdiet
                nulla malesuada pellentesque elit eget gravida cum sociis
                natoque. Placerat duis ultricies lacus sed. Enim ut tellus
                elementum sagittis vitae. Mattis nunc sed blandit libero
                volutpat sed cras. Amet risus nullam eget felis eget nunc
                lobortis. Gravida in fermentum et sollicitudin ac orci
                phasellus. Commodo viverra maecenas accumsan lacus. Aliquam
                malesuada bibendum arcu vitae elementum curabitur. Porttitor
                massa id neque aliquam. Pellentesque elit ullamcorper dignissim
                cras. Consequat nisl vel pretium lectus. Mattis vulputate enim
                nulla aliquet porttitor lacus luctus.
              </p>
            </Tab>
          </Tabs>
        </div>
      </div>

      <Footer />
    </div>
  );
}
