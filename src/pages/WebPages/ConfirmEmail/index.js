import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { MdCheckCircle, MdError } from "react-icons/md";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";
import Spinner from "../../../components/UI/Spinner/Spinner";

import api from "../../../services/api";

import "./styles.css";

export default function RecoverPassword() {
  const location = useLocation();

  const [confirmed, setConfirmed] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    async function fetchConfirmation() {
      const token = location.search.replace("?token=", "");

      try {
        if (!token) {
          throw new Error();
        }

        await api.post("email/confirm", { token });
        setConfirmed(true);
      } catch (error) {
        setError(true);
        console.log(error);
      }
    }

    fetchConfirmation();
  }, [location.search]);

  let modalDataSended = (
    <div className="modalConfirmEmail">
      <MdCheckCircle size={100} color="#34cc20" />
      <p>Email confirmado com sucesso</p>
    </div>
  );

  let modalDataError = (
    <div className="modalConfirmEmail">
      <MdError size={100} color="#e02041" />
      <p>Erro ao confirmar o email</p>
    </div>
  );

  return (
    <div>
      <header>
        <Header />
      </header>

      <div className="confirmEmailContainer">
        <div className="confirmEmailContent">
          {confirmed ? modalDataSended : error ? modalDataError : <Spinner />}
        </div>
      </div>

      <footer>
        <Footer />
      </footer>
    </div>
  );
}
