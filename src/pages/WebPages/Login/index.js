import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { Form, Input } from "@rocketseat/unform";

import { useAuth } from "../../../auth/auth";

import Logo from "../../../assets/images/logo.png";

import "./styles/login.css";
import "./styles/loginResponsive.css";

//configura o tempo de duraçaõ do toast
toast.configure({
  autoClose: 3000,
  draggable: false,
});

//validação do formulário com yup
const schema = Yup.object().shape({
  email: Yup.string()
    .email()
    .required("É obrigatório informar um email válido"),
  password: Yup.string().required("É obrigatório informar uma senha"),
});

export default function Login() {
  const [email, setEmail] = useState("");
  const [passwordHash, setpasswordHash] = useState("");

  const { signIn, user } = useAuth();

  const history = useHistory();

  async function handleLogin() {
    try {
      await signIn({
        email,
        passwordHash,
      });

      if (user.role === "ENTREPRENEUR") {
        history.push("/entrepreneurHome");
      }
      if (user.role === "SUPPORTER") {
        history.push("/investorHome");
      }
      if (user.role === "PARTNER") {
        history.push("/partnerHome");
      }
    } catch (err) {
      toast.error("Falha ao fazer login, Email ou Senha incorretos.");
      setpasswordHash("");
    }
  }

  return (
    <div id="loginContainer">
      <img src={Logo} alt="Logo da Firgun" />
      <Form onSubmit={handleLogin} schema={schema}>
        <div className="loginContent">
          <label htmlFor="email">
            Email
            <Input
              id="email"
              name="email"
              type="email"
              placeholder="Digite seu e-mail"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>

          <label htmlFor="password">
            Senha
            <Input
              id="password"
              name="password"
              type="password"
              placeholder="Digite sua senha"
              value={passwordHash}
              onChange={(e) => setpasswordHash(e.target.value)}
            />
          </label>
          <button type="submit">Entrar</button>

          <p>
            Esqueceu sua senha?
            <font color="#3ba6ff">
              <Link to="/recoverPassword">Clique aqui.</Link>
            </font>
          </p>
          <p>
            Ainda não tem cadastro na Firgun?
            <font color="#3ba6ff">
              <Link to="/chooseRegister">Clique aqui.</Link>
            </font>
          </p>
        </div>
      </Form>
    </div>
  );
}
