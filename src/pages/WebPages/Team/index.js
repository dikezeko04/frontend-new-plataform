import React, { useEffect, useState } from "react";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";
import TeamCard from "../../../components/teamCard/TeamCard";

import api from "../../../services/api";

import "./styles/team.css";
import "./styles/teamResponsive.css";

export default function Team() {
  const [teamMembers, setTeamMember] = useState([]);

  useEffect(() => {
    api.get("admin/firgun-team").then((response) => {
      setTeamMember(response.data);
    });
  }, []);

  return (
    <div id="team">
      <Header />
      <div id="teamMain">
        <h2>nosso time</h2>
        <div className="teamCards">
          {teamMembers.map((teamMember) => (
            <TeamCard
              key={teamMember.id}
              name={teamMember.firstName + " " + teamMember.lastName}
              avatar={teamMember.avatarUrl}
              occupation={teamMember.occupation}
              email={teamMember.email}
              linkedin={teamMember.linkedin}
            />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
}
