import React, { useEffect, useState } from "react";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";
import Timeline from "../../../components/timeline/Timeline";

import api from "../../../services/api";

import "./styles/about.css";
import "./styles/aboutResponsive.css";

import Award from "../../../assets/images/award.jpg";

export default function About() {
  const [info, setinfo] = useState({});

  useEffect(() => {
    api.get("admin/about-firgun").then((response) => {
      setinfo(response.data);
    });
  }, []);

  return (
    <div>
      <Header />

      <div id="main">
        <div id="banner"></div>

        <h3>Nossa história</h3>

        <div className="text">
          <p>{info.about}</p>
        </div>

        <div className="storyLine">
          <Timeline />
        </div>

        <div className="purpose">
          <div className="field">
            <h4>Por que criamos a Firgun?</h4>
            <p>{info.whyCreated}</p>
          </div>

          <div className="field">
            <h4>Oque fazemos?</h4>
            <p>{info.whatWeDo}</p>
          </div>
        </div>

        <h3>Premiações</h3>

        <div className="awards">
          <div className="textAwards">
            <div className="text1">
              <h4>Iniciativa Incluir</h4>
              <p>{info.awards1}</p>
            </div>

            <div className="text2">
              <h4>Aceleradoras</h4>
              <p>{info.awards2}</p>
            </div>
          </div>

          <img src={Award} alt="" />
        </div>
      </div>

      <Footer />
    </div>
  );
}
