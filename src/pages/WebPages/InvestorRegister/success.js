import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/investorThree.css";

export default function InvestorRegisterFour() {
  const now = 100;
  return (
    <div id="investorFour">
      <Header />
      <div className="mainInvestFour">
        <h3>Cadastro de investidor - 5/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar animated now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentInvestFour">
          <h2>Cadastro realizado com sucesso!</h2>
        </div>
      </div>
      <Footer />
    </div>
  );
}
