import React from "react";
import InputMask from "react-input-mask";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Link } from "react-router-dom";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/investorOne.css";

export default function InvestorRegister() {
  const now = 25;
  return (
    <div id="investorOne">
      <Header />
      <div className="mainInvestOne">
        <h3>Cadastro de investidor - 1/4</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentInvestOne">
          <form action="">
            <label htmlFor="">Nome:</label>
            <input type="text" />

            <label htmlFor="">Sobrenome:</label>
            <input type="text" />

            <label htmlFor="">E-mail:</label>
            <input type="text" />

            <label htmlFor="">CPF:</label>
            <InputMask mask="999.999.999-99" type="text" />

            <label htmlFor="">Data de nascimento:</label>
            <InputMask mask="99/99/9999" type="text" />

            <label htmlFor="">Gênero:</label>
            <select name="" id="">
              <option value="selected">Selecione</option>
              <option value="">Masculino</option>
              <option value="">Feminino</option>
              <option value="">Não-binário</option>
            </select>

            <label htmlFor="">Raça:</label>
            <select name="" id="">
              <option value="selected">Preto</option>
              <option value="">Pardo</option>
              <option value="">Branco</option>
              <option value="">Amarelo</option>
              <option value="">Outro</option>
            </select>
          </form>

          <button>
            <Link to="/investorRegister/stepTwo" style={{ color: "white" }}>
              Continuar
            </Link>
          </button>
        </div>
      </div>
      <Footer />
    </div>
  );
}
