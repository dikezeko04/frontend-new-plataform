import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Link } from "react-router-dom";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/investorFour.css";

export default function InvestorRegisterThree() {
  const now = 75;
  return (
    <div id="investorThree">
      <Header />
      <div className="mainInvestThree">
        <h3>Cadastro de investidor - 3/4</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentInvestThree">
          <form action="">
            <label htmlFor="">Senha:</label>
            <input type="password" />

            <label htmlFor="">Confirmar senha:</label>
            <input type="password" />
          </form>

          <div className="btnWay">
            <button>
              <Link to="/investorRegister/stepTwo" style={{ color: "white" }}>
                Voltar
              </Link>
            </button>

            <button>
              <Link to="/investorRegister/success" style={{ color: "white" }}>
                Continuar
              </Link>
            </button>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
