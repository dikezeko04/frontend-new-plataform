import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Link } from "react-router-dom";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/partnerFour.css";

export default function PartnerRegisterFour() {
  const now = 80;
  return (
    <div id="partnerFour">
      <Header />
      <div className="mainPartFour">
        <h3>Cadastro de parceiro - 4/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar animated now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentPartFour">
          <form action="">
            <label htmlFor="">Senha:</label>
            <input type="password" />

            <label htmlFor="">Confirmar senha:</label>
            <input type="password" />
          </form>

          <div className="btnWay">
            <button>
              <Link to="/partnerRegister/stepThree">Voltar</Link>
            </button>

            <button>
              <Link to="/partnerRegister/success">Continuar</Link>
            </button>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
