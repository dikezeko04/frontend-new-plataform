import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/partnerFour.css";

export default function EntrepreneurRegisterFour() {
  const now = 100;
  return (
    <div id="partnerFour">
      <Header />
      <div className="mainPartFour">
        <h3>Cadastro de empreendedor - 5/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar animated now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentPartFour">
          <h2>Cadastro realizado com sucesso!</h2>
        </div>
      </div>
      <Footer />
    </div>
  );
}
