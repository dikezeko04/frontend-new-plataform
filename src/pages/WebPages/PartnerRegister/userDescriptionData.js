import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Link } from "react-router-dom";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/partnerThree.css";

export default function EntrepreneurRegisterTwo() {
  const now = 60;
  return (
    <div id="partnerThree">
      <Header />
      <div className="mainPartThree">
        <h3>Cadastro de parceiro - 3/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar animated now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentPartThree">
          <form action="">
            <label htmlFor="">Descrição:</label>
            <textarea
              name=""
              id=""
              cols="5"
              rows="5"
              placeholder="Escreva uma descrição de até 120 caracteres"
            />

            <label htmlFor="">Site:</label>
            <input type="text" />
          </form>

          <div className="btnWay">
            <button className="btn">
              <Link to="/partnerRegister/stepTwo">Voltar</Link>
            </button>

            <button>
              <Link to="/partnerRegister/stepFour">Continuar</Link>
            </button>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
