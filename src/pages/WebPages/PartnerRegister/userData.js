import React from "react";
import InputMask from "react-input-mask";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Link } from "react-router-dom";

import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";

import "./styles/partnerOne.css";

export default function PartnerRegister() {
  const now = 20;
  return (
    <div id="partOne">
      <Header />
      <div className="mainPartOne">
        <h3>Cadastro de parceiro - 1/5</h3>
        <div className="levels">
          <ProgressBar id="bar">
            <ProgressBar animated now={now} label={`${now}%`} />
          </ProgressBar>
        </div>
        <div className="contentPartOne">
          <form action="">
            <label htmlFor="">Nome:</label>
            <input type="text" />

            <label htmlFor="">Sobrenome:</label>
            <input type="text" />

            <label htmlFor="">E-mail:</label>
            <input type="text" />

            <label htmlFor="">CPF:</label>
            <InputMask mask="999.999.999-99" type="text" />

            <label htmlFor="">Data de nascimento:</label>
            <InputMask mask="99/99/9999" type="text" />
          </form>

          <button>
            <Link to="/partnerRegister/stepTwo" className="btn">
              Continuar
            </Link>
          </button>
        </div>
      </div>
      <Footer />
    </div>
  );
}
