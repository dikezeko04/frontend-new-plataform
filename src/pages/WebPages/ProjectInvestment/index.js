import React, { useState, useEffect } from "react";
import { FaCreditCard, FaBarcode, FaTicketAlt } from "react-icons/fa";
import { Link, useLocation } from "react-router-dom";
import { differenceInCalendarDays, parseISO } from "date-fns";
import { MdError, MdCheckCircle } from "react-icons/md";
import { Form, Input } from "@rocketseat/unform";
import * as Yup from "yup";

import ProgressBar from "react-bootstrap/ProgressBar";
import Header from "../../../components/header/Header";
import Footer from "../../../components/footer/Footer";
import Modal from "../../../components/UI/Modal/Modal";
import Spinner from "../../../components/UI/Spinner/Spinner";

import api from "../../../services/api";

import Teste from "../../../assets/images/teste.jpg";

import "./styles/projectInvest.css";
import "./styles/projectInvestResponsive.css";

//validação do formulário com yup
const schemaCredit = Yup.object().shape({
  cardNumber: Yup.string().required("Obrigatório informar o número do cartão"),
  titularName: Yup.string().required("Nome do titular é obrigatório"),
  value: Yup.string().required("É obrigatório ter um valor"),
  cardMonth: Yup.string().required("É obrigatório informar o mês"),
  cardYear: Yup.string().required("É obrigatório informar o ano"),
  cvc: Yup.string().required("É obrigatório informar o CVC"),
});
const valueSchema = Yup.object().shape({
  value: Yup.string().required("É obrigatório ter um valor"),
});

export default function ProjectInvestment() {
  const location = useLocation();

  const [boletoButton, setBoletoButton] = useState(false);
  const [creditButton, setCreditButton] = useState(false);
  const [purchasing, setPurchasing] = useState(false);
  const [firgunButton, setFirgunButton] = useState(false);
  const [value, setValue] = useState("");
  const [titularName, setTitularName] = useState("");
  const [cardNumber, setCardNumber] = useState("");
  const [cardMonth, setCardMonth] = useState("");
  const [cardYear, setCardYear] = useState("");
  const [cvc, setCvc] = useState("");
  const [payed, setPayed] = useState(false);
  const [error, setError] = useState(false);
  const [boletoPayment, setBoletoPayment] = useState(false);
  const [project, setProject] = useState({});
  const [project_id, setProjectId] = useState("");
  const [user, setUser] = useState({});
  const [paymentData, setPaymentData] = useState({});
  const [userBalance, setUserBalance] = useState({});

  useEffect(() => {
    async function fetchData() {
      const projectId = location.search.replace("?project=", "");
      setProjectId(projectId);
      try {
        if (!projectId) {
          throw new Error();
        }

        const response = await api.get(
          `projects/get-project?project_id=${projectId}`
        );
        const balance = await api.get("balances");

        setProject(response.data);
        setUser(response.data.__entrepreneur__);
        setUserBalance(balance.data);
      } catch (error) {
        console.log(error);
      }
    }

    fetchData();
  }, [location.search, paymentData, payed]);

  const purchaseCancelHandler = () => {
    setPurchasing(false);
  };

  const handleBoletoButton = () => {
    setBoletoButton(!boletoButton);
    setCreditButton(false);
    setFirgunButton(false);
  };
  const handleCreditButton = () => {
    setCreditButton(!creditButton);
    setBoletoButton(false);
    setFirgunButton(false);
  };
  const handleFirgunButton = () => {
    setFirgunButton(!firgunButton);
    setBoletoButton(false);
    setCreditButton(false);
  };

  const handleBoletoPayment = async () => {
    try {
      setPayed(false);
      setPurchasing(!purchasing);
      const data = {
        value,
        confirmationToShowPhoto: true,
        paymentType: "BOLETO",
      };

      const response = await api.post(
        `payments/investiment?project_id=${project_id}`,
        data
      );

      setPaymentData(response.data);
      setBoletoPayment(true);
      setValue("");
    } catch (err) {
      setError(true);
      console.log(err);
    }
  };

  const handleCardPayment = async () => {
    try {
      setBoletoPayment(false);

      setPurchasing(!purchasing);
      const data = {
        value,
        name: titularName,
        taxDocumentType: user.taxDocumentType,
        taxDocumentNumber: user.taxDocumentNumber,
        creditCardCvc: cvc,
        birthdate: user.birthdate,
        creditCardNumber: cardNumber,
        creditCardExpirationMonth: cardMonth,
        creditCardExpirationYear: cardYear,
        paymentType: "CREDIT_CARD",
      };

      await api.post(`payments/investiment?project_id=${project_id}`, data);
      setPayed(true);
      setValue("");
      setTitularName("");
      setCardNumber("");
      setCardMonth("");
      setCardYear("");
      setCvc("");
    } catch (err) {
      setError(true);
      console.log(err);
    }
  };

  const handleFirgunPayment = async () => {
    try {
      setBoletoPayment(false);

      setPurchasing(!purchasing);

      await api.post(`payments/firgun-investiment?project_id=${project_id}`, {
        value,
      });

      setPayed(true);
      setValue("");
    } catch (err) {
      setError(true);
      console.log(err);
    }
  };

  let modalDataPayed = (
    <div className="modalPayment">
      <MdCheckCircle size={100} color="#34cc20" />
      <p>Investimento feito com sucesso</p>
    </div>
  );

  let modalDataError = (
    <div className="modalPayment">
      <MdError size={100} color="#e02041" />
      <p>Erro ao investir no projeto</p>
    </div>
  );

  let modalDataBoleto = (
    <div className="">
      <MdCheckCircle size={80} color="#34cc20" />
      <h5>Investimento feito com sucesso</h5>

      <p>Abaixo segue o código do boleto:</p>
      <p>{paymentData.lineCode}</p>
      <p>
        <strong>Preço Total: R$ {paymentData.amount}</strong>
      </p>
      <p>Parcela: {paymentData.installments}</p>
      <button className="modalDataLoadedClose" onClick={purchaseCancelHandler}>
        FECHAR
      </button>
      <Link
        to={{
          pathname: paymentData.boletoLink,
        }}
        target="blank"
      >
        <button className="modalDataLoadedPrint">IMPRIMIR BOLETO</button>
      </Link>
    </div>
  );

  const percentage = (Number(project.raised) / Number(project.goal)) * 100;

  return (
    <div id="projectInvest">
      <Header />

      <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
        {payed ? (
          modalDataPayed
        ) : boletoPayment ? (
          modalDataBoleto
        ) : error ? (
          modalDataError
        ) : (
          <Spinner />
        )}
      </Modal>

      <div id="mainProject">
        <div className="investmentContainer">
          <div className="containerLeft">
            <div className="paymentType">
              <span>Selecione método de pagamento</span>
              <div className="boxPaymenType">
                <div
                  className={firgunButton ? "box boxActive" : "box"}
                  onClick={handleFirgunButton}
                >
                  <FaTicketAlt size={80} />
                </div>
                <div
                  className={creditButton ? "box boxActive" : "box"}
                  onClick={handleCreditButton}
                >
                  <FaCreditCard size={80} />
                </div>
                <div
                  className={boletoButton ? "box boxActive" : "box"}
                  onClick={handleBoletoButton}
                >
                  <FaBarcode size={80} />
                </div>
              </div>
            </div>

            {firgunButton ? (
              <div className="boxAmount">
                <p>Valor a investir</p>
                <Form onSubmit={handleFirgunPayment} schema={valueSchema}>
                  <div className="amount">
                    <p>Seu Saldo: R$ {userBalance.current},00</p>
                    <label htmlFor="value">
                      R$*
                      <Input
                        type="text"
                        name="value"
                        id="value"
                        placeholder="Digite um valor"
                        value={value}
                        onChange={(e) => setValue(e.target.value)}
                      />
                    </label>
                  </div>
                  <button type="submit">APOIAR COM SALDO FIRGUN</button>
                </Form>
              </div>
            ) : (
              ""
            )}

            {creditButton ? (
              <div className="creditCardGroup">
                <Form onSubmit={handleCardPayment} schema={schemaCredit}>
                  <div className="formGroupCredit">
                    <label htmlFor="cardNumber">Número do cartão*</label>
                    <Input
                      type="text"
                      name="cardNumber"
                      id="cardNumber"
                      placeholder="Número do cartão"
                      value={cardNumber}
                      onChange={(e) => setCardNumber(e.target.value)}
                    />
                  </div>
                  <div className="formGroupCredit">
                    <label htmlFor="titularName">Nome do titular*</label>
                    <Input
                      type="text"
                      name="titularName"
                      id="titularName"
                      placeholder="Nome do titular"
                      value={titularName}
                      onChange={(e) => setTitularName(e.target.value)}
                    />
                  </div>
                  <div className="formGroupCredit">
                    <label htmlFor="cpf">Documento do titular*</label>
                    <Input
                      type="text"
                      name="cpf"
                      id="cpf"
                      className="inputBlocked"
                      disabled={true}
                      value={user.taxDocumentNumber}
                      placeholder="Documento do titular"
                    />
                  </div>
                  <div className="formGroupCredit">
                    <label htmlFor="cardMonth">Mês de vencimento*</label>
                    <Input
                      type="text"
                      name="cardMonth"
                      id="cardMonth"
                      placeholder="Mês vencimento"
                      value={cardMonth}
                      onChange={(e) => setCardMonth(e.target.value)}
                    />
                    <label htmlFor="cardYear">Ano de vencimento*</label>
                    <Input
                      type="text"
                      name="cardYear"
                      id="cardYear"
                      placeholder="Número do cartão"
                      value={cardYear}
                      onChange={(e) => setCardYear(e.target.value)}
                    />
                  </div>
                  <div className="formGroupCredit">
                    <label htmlFor="cvc">CVC*</label>
                    <Input
                      type="text"
                      name="cvc"
                      id="cvc"
                      placeholder="CVC"
                      value={cvc}
                      onChange={(e) => setCvc(e.target.value)}
                    />
                  </div>
                  <div className="formGroupCredit">
                    <label htmlFor="cvc">Valor a investir (R$)*</label>
                    <Input
                      type="text"
                      name="value"
                      id="value"
                      placeholder="Digite um valor"
                      value={value}
                      onChange={(e) => setValue(e.target.value)}
                    />
                  </div>
                  <button type="submit">APOIAR COM CARTÃO DE CRÉDITO</button>
                </Form>
              </div>
            ) : (
              ""
            )}

            {boletoButton ? (
              <div className="boxAmount">
                <p>Valor a investir</p>
                <Form onSubmit={handleBoletoPayment} schema={valueSchema}>
                  <div className="amount">
                    <label htmlFor="value">
                      R$*
                      <Input
                        type="text"
                        name="value"
                        id="value"
                        placeholder="Digite um valor"
                        value={value}
                        onChange={(e) => setValue(e.target.value)}
                      />
                    </label>
                  </div>
                  <button type="submit">APOIAR COM BOLETO</button>
                </Form>
              </div>
            ) : (
              ""
            )}
          </div>

          <div className="containerRight">
            <div className="investmentProject">
              <div className="entrepreneur">
                <img
                  src={!user.avatarUrl ? Teste : user.avatarUrl}
                  alt={user.firstName}
                />
                <span>{user.firstName + " " + user.lastName}</span>
              </div>

              <div className="projectData">
                <div className="projectDataItem dataBorder">
                  <span>
                    <b>6%</b>
                  </span>
                  <p>ao ano</p>
                </div>
                <div className="projectDataItem dataBorder">
                  <span>
                    <b>{project.totalSupporters}</b>
                  </span>
                  <p>heróis</p>
                </div>
                <div className="projectDataItem dataBorder">
                  <span>
                    <b>
                      {`${differenceInCalendarDays(
                        parseISO(String(project.dateLimit)),
                        Date.now()
                      )}`}
                    </b>
                  </span>
                  <p>dias</p>
                </div>
                <div className="projectDataItem">
                  <span>
                    <b>{project.installmentsPrediction}</b>
                  </span>
                  <p>parcelas</p>
                </div>
              </div>
              <p className="projectName">{project.name}</p>
              <div className="progressBar">
                <div className="projectGoal">
                  <p>R$ {project.raised}</p>
                  <p>R$ {project.goal},00</p>
                </div>
                <ProgressBar
                  id="bar"
                  animated
                  now={percentage}
                  label={`${percentage}%`}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
