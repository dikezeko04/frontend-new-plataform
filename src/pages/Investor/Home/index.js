import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import { useHistory } from "react-router-dom";

import HeaderDash from "../../../components/headerDash/HeaderDash";
import Menu from "../../../components/menuInvestor/Menu";
import MenuMobile from "../../../components/menuMobile/MenuMobile";
import Chart from "../../../components/chart/LineChart/Chart";

import api from "../../../services/api";

import { useAuth } from "../../../auth/auth";

import "./styles/investor.css";

export default function InvestorHome() {
  const { user } = useAuth();

  const history = useHistory();

  if (!user) history.push("/");

  const [generalStatement, setGeneralStatement] = useState({});

  useEffect(() => {
    api.get("supporters/general-statement").then((response) => {
      setGeneralStatement(response.data);
    });
  }, []);
  return (
    <>
      <MenuMobile />
      <div id="investorHome">
        <Menu />
        <div className="content">
          <HeaderDash />

          <div className="homeContent">
            <Chart
              investedInMoment={generalStatement.investedInMoment}
              amountToReceiveInMoment={generalStatement.amountToReceiveInMoment}
              amountRetiredInMoment={generalStatement.amountRetiredInMoment}
            />
            <div className="homeTable">
              <Table responsive>
                <thead>
                  <tr>
                    <th>Projeto</th>
                    <th>Total investido</th>
                    <th>Total recebido</th>
                    <th>Total a receber</th>
                    <th>rendimento</th>
                    <th>Último recebimento</th>
                    <th>Próximo recebimento</th>
                    <th>Próximo valor</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{generalStatement.projectName}</td>
                    <td>R$ {generalStatement.totalInvested}</td>
                    <td>R$ {generalStatement.totalReceived}</td>
                    <td>R$ {generalStatement.amountToReceive}</td>
                    <td>R$ 8.000,00</td>
                    <td>{generalStatement.lastReceivementDate}</td>
                    <td>{generalStatement.nextReceivementDate}</td>
                    <td>R$ {generalStatement.nextReceivementAmount}</td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
