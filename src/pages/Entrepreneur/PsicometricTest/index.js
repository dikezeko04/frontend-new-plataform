import React, { useState, useEffect, useCallback } from "react";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import ProgressBar from "react-bootstrap/ProgressBar";
import { MdCheckCircle, MdError } from "react-icons/md";

import HeaderDash from "../../../components/headerDash/HeaderDash";
import Menu from "../../../components/menuEntrepreneur/Menu";
import MenuMobile from "../../../components/menuMobile/MenuMobile";
import Modal from "../../../components/UI/Modal/Modal";

import api from "../../../services/api";
import { useAuth } from "../../../auth/auth";

import "./styles/styles.css";

//configura o tempo de duraçaõ do toast
toast.configure({
  autoClose: 3000,
  draggable: false,
});

export default function EntrepreneurProject() {
  const { user } = useAuth();

  const history = useHistory();

  if (!user) history.push("/");

  const [quiz, setQuiz] = useState([]);
  const [answered, setAnswered] = useState(0);
  const [score, setScore] = useState(0);
  const [purchasing, setPurchasing] = useState(false);
  const [questionNumber, setQuestionNumber] = useState(1);
  const [answerDate, setAnswerDate] = useState("");
  const [inputName, setInputName] = useState([]);
  const [points, setPoints] = useState([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    api.get("admin/quiz").then((response) => {
      setQuiz(response.data);
      // setQuestionNumber(response.data[0].question.id);
    });
    api.get("users/profile").then((user) => {
      setAnswerDate(user.data.quizAnswerDate);
    });
  }, [loaded]);

  const purchaseCancelHandler = () => {
    setPurchasing(false);
  };

  const percentage = (answered / quiz.length) * 100;

  const handleAnswerCounter = (name, point) => {
    setInputName(name);

    const checked = inputName.includes(name);

    if (checked) {
      points.forEach((item) =>
        item.pointData.name === name
          ? (item.pointData.point = point)
          : item.pointData.point
      );
    }
    if (!checked) {
      setAnswered(answered + 1);

      const data = {
        pointData: {
          name,
          point,
        },
      };
      setPoints([...points, data]);
    }
  };

  const handleAnswerQuiz = useCallback(async () => {
    let quizScore = 0;
    try {
      points.map(
        (data) => (quizScore = quizScore + Number(data.pointData.point))
      );

      await api.post("entrepreneurs/quiz", { quizScore: String(quizScore) });
      setScore(quizScore);
      toast.success("Questionário respondido com sucesso");
      setPurchasing(!purchasing);
      setLoaded(!loaded);
    } catch (error) {
      toast.error("Erro ao responder o questianário");
      console.log(error);
    }
  }, [points, loaded, purchasing]);

  const handleIncrement = () => {
    setQuestionNumber(questionNumber + 1);
  };

  const handleDecrement = () => {
    setQuestionNumber(questionNumber - 1);
  };

  // Quando o projeto é criado
  let modalDataAproved = (
    <div className="modalTestData">
      <MdCheckCircle size={100} color="#34cc20" />
      <p>
        Você passou no nosso teste psicométrico com score de:{" "}
        <strong>{score}</strong>
      </p>
      <p>Verifique o seu email para mais informações.</p>
    </div>
  );
  let modalDataReproved = (
    <div className="modalTestData">
      <MdError size={100} color="#e02041" />
      <p>
        Você não passou no nosso teste psicométrico teve um score de:{" "}
        <strong>{score}</strong>
      </p>
      <p>Verifique o seu email para mais informações.</p>
    </div>
  );

  return (
    <>
      <MenuMobile />
      <div id="psicometricTest">
        <Menu />

        <div className="content">
          <HeaderDash alterLink="/entrepreneurData" />
          <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
            {score > 70 ? modalDataAproved : modalDataReproved}
          </Modal>
          {!answerDate ? (
            <>
              <h3>
                Pergunta {questionNumber}/{quiz.length}
              </h3>
              <div className="levels">
                <ProgressBar id="bar">
                  <ProgressBar
                    now={percentage}
                    label={`${percentage.toFixed(2)}%`}
                  />
                </ProgressBar>
              </div>
              <div className="quizContainer">
                <div className="quizQuestion">
                  {quiz.map((item) =>
                    item.question.id === questionNumber ? (
                      <div className="quizAnswers" key={item.question.id}>
                        <p>{item.question.question}</p>
                        {item.answers.map((answerItem) => (
                          <label key={answerItem.id} htmlFor={answerItem.id}>
                            {answerItem.answer}
                            <input
                              id={answerItem.id}
                              name={item.question.id}
                              type="radio"
                              // checked={}
                              onChange={(e) =>
                                handleAnswerCounter(
                                  e.target.name,
                                  answerItem.point
                                )
                              }
                            />
                          </label>
                        ))}
                      </div>
                    ) : (
                      ""
                    )
                  )}
                </div>
                <div className="iterableButtons">
                  <button
                    onClick={handleDecrement}
                    className={questionNumber > 1 ? "" : "previuos"}
                  >
                    Anterior
                  </button>
                  <button
                    onClick={handleIncrement}
                    className={questionNumber === quiz.length ? "next" : ""}
                  >
                    Próximo
                  </button>
                  <button
                    onClick={handleAnswerQuiz}
                    className={questionNumber === quiz.length ? "" : "confirm"}
                  >
                    Confirmar
                  </button>
                </div>
              </div>
            </>
          ) : (
            <p className="empty">Você já respondeu o questianário</p>
          )}
        </div>
      </div>
    </>
  );
}
