import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import "./styles/header.css";
import "./styles/headerResponsive.css";

import Logo from "../../assets/images/logo.png";

export class Header extends Component {
  render() {
    return (
      <div id="head">
        <header>
          <img src={Logo} alt="Logo da Firgun" />
          <ul>
            <NavLink to="/about" className="link" activeClassName="active">
              sobre
            </NavLink>
            <NavLink to="/projects" className="link" activeClassName="active">
              projetos
            </NavLink>
            <NavLink to="/partners" className="link" activeClassName="active">
              parceiros
            </NavLink>
            <NavLink to="/blog" className="link">
              blog
            </NavLink>
            <NavLink to="/contact" className="link" activeClassName="active">
              contato
            </NavLink>
            <NavLink to="/team" className="link" activeClassName="active">
              time
            </NavLink>
          </ul>
          <NavLink className="headerLogin" to="/login">
            login
          </NavLink>
        </header>
      </div>
    );
  }
}

export default Header;
