import React from "react";
import { Line } from "react-chartjs-2";

export default function Chart(props) {
  const chartData = {
    labels: [
      "Investido no momento",
      "Á receber",
      "Valor reinvestido",
      "Valor retirado",
    ],
    datasets: [
      {
        label: "Investimento mensal",
        data: [
          Number(props.investedInMoment),
          Number(props.amountToReceiveInMoment),
          0,
          Number(props.amountRetiredInMoment),
          220,
        ],
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(75,192,192,1)",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgba(75,192,192,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 5,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
      },
    ],
  };

  return (
    <div className="chart">
      <Line
        data={chartData}
        width={600}
        height={400}
        type={Line}
        options={{
          responsive: true,
          maintainAspectRatio: false,
        }}
      />
    </div>
  );
}
