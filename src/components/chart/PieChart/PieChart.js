import React, { Component } from "react";
import { Pie } from "react-chartjs-2";

export class PieChart extends Component {
  render() {
    return (
      <div className="pieChart">
        <Pie />
      </div>
    );
  }
}
