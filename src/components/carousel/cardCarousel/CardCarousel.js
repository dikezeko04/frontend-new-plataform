import React from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { FaClock, FaUserFriends } from "react-icons/fa";
import { differenceInCalendarDays, parseISO } from "date-fns";

import Teste from "../../../assets/images/teste.jpg";

import "./cardCarousel.css";
import "./cardCarouselResponsive.css";

function CardCarousel(props) {
  const now = (Number(props.raised) / Number(props.goal)) * 100;
  return (
    <>
      <div className="cardCarouselContainer">
        <img src={!props.photo ? Teste : props.photo} alt={props.name} />
        <h5>{props.entrepreneurName}</h5>
        <h5>{props.name}</h5>
        <p className="tP">{props.description}</p>

        <ProgressBar
          id="barCard"
          animated
          variant="success"
          now={now}
          label={`${now}%`}
        />

        <div className="cardBorder">
          <div className="cardTime">
            <p>
              <FaClock className="icon" />
              {differenceInCalendarDays(
                parseISO(String(props.date)),
                Date.now()
              )}{" "}
              dias
            </p>
          </div>

          <div className="cardMoney">
            <p>R$ {props.goal},00</p>
          </div>

          <div className="cardInvestors">
            <p>
              <FaUserFriends className="icon" />
              {props.totalInvestments}
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default CardCarousel;
