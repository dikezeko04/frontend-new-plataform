import React, { Component } from "react";

import "./styles/partnerCard.css";
import "./styles/partnerCardResponsive.css";

import Teste from "../../assets/images/teste.jpg";

export class PartnerCard extends Component {
  render() {
    const partner = this.props;

    return (
      <div id="partCard">
        <div className="tp">
          <img
            src={partner.avatar ? partner.avatar : Teste}
            alt={partner.name}
          />
          <div className="box">
            <h3>{partner.name}</h3>
            <p>{partner.city}</p>
          </div>
        </div>
        <div className="bt">
          <div className="bx">
            <p>{!partner.description ? "" : partner.description}</p>
          </div>
          <button>saiba mais</button>
        </div>
      </div>
    );
  }
}

export default PartnerCard;
