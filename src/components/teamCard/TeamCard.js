import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { FaLinkedinIn, FaEnvelope } from "react-icons/fa";

import "./styles/teamCard.css";
import "./styles/teamCardResponsive.css";

import Teste from "../../assets/images/teste.jpg";

export class TeamCard extends Component {
  render() {
    const member = this.props;

    return (
      <div id="cards">
        <img src={!member.avatar ? Teste : member.avatar} alt={member.name} />
        <h4>{member.name}</h4>
        <p>{member.occupation}</p>

        <div className="cardIcons">
          <NavLink
            to={{
              pathname: member.linkedin,
            }}
            target="blank"
          >
            <FaLinkedinIn className="icons" size={30} />
          </NavLink>
          <NavLink
            to={{
              pathname: member.linkedin,
            }}
            target="blank"
          >
            <FaEnvelope className="icons" size={30} />
          </NavLink>
        </div>
      </div>
    );
  }
}

export default TeamCard;
